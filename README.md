# Bob-api

## Purpose

A simple dashboard for GitLab build on Rust.

## Name

Named after Bob the Builder

## Usage

### Start database

Start the database with credentials: `root` / `example`
```
docker-compose up -d
```

There's an additional webui for the database: http://localhost:8080

### Set .env

Set the database connection url in the `.env` file. A `.env_example` is available for hints.


### Run setup of database
```
cargo install diesel_cli
```

```
diesel setup
```

```
diesel migration run
```

### Start Bob

  Alert: For now you need to manually set the DATABASE_URL in your environment to run cargo.

``` fish
set -x DATABASE_URL mysql://root:example@127.0.0.1:3306/heroes
```

```
cargo run
```

### Start quering

```
http GET localhost:8000/heroes
```

See cargo log for other available urls

## Inspiration

This project was initial taken from [here](https://medium.com/sean3z/building-a-restful-crud-api-with-rust-1867308352d8) and updated to the 'latest' cargo packages.
[github](https://github.com/sean3z/rocket-diesel-rest-api-example/)
